﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class Evaluation {

	// reaction time (in minutes)
	public float reactionTime = .2f / 60f;  // 1/5 of a second

	public Evaluation(UserInput userInput, Clock clock, Song song, Targets targets, Player player) {
		// save parameters
		this.userInput = userInput;
		this.clock = clock;
		this.song = song;
		this.targets = targets;
		this.player = player;
	}

	public void Update() {
		// update checkpoint
		this.UpdateCheckpoint();

		// get instruments map for drum track
		Dictionary<int, int> instrumentsMap = this.song.InstrumentsMap(0);

		// get starting and ending note time to consider (within reaction time around current time)
		float reactionTicks = (float)(this.reactionTime * this.clock.tempo * this.clock.ticksPerQuarterNote);
		float windowStart = Mathf.Max(Mathf.Floor((float)(this.clock.time - reactionTicks)), 0);
		float windowEnd = Mathf.Min(Mathf.Ceil((float)(this.clock.time + reactionTicks)), this.song.Duration(0));

		// make a local copy of played instruments
		List<int> playedInstruments = this.userInput.playedInstruments;

		// get drum track
		List<Song.Note> notes = this.song.tracks[0];

		// for each note
		for (int i = 0; i < notes.Count; i++) {
			// get note
			Song.Note note = notes[i];

			// get target
			Target target = this.targets.targets[i];

			// if note has passed reaction window and its target is not played, report miss
			if (note.startTime < windowStart && !target.played) {
				// if note is after last missed note time
				if (note.startTime > this.lastMissedNoteTime) {
					// report miss
					this.MissedNote(note, target);

					// update last missed note time
					this.lastMissedNoteTime = note.startTime;
				}

				// skip it
				continue;
			}

			// if note is not within reaction window, skip it
			if (note.startTime < windowStart || note.startTime > windowEnd) continue;

			// for each played instrument
			foreach (int idx in playedInstruments) {
				// if note is not played instrument, skip it
				if (instrumentsMap[note.noteNumber] != idx) continue;

				// if target is not yet played
				if (!target.played) {
					// mark target as played
					target.played = true;

					// remove instrument from played instruments
					playedInstruments.Remove(idx);

					// no need to check any more instruments
					break;
				}
			}
		}
	}

	protected void UpdateCheckpoint() {
		// compute current measure
		double measure = this.clock.time / 4 / this.clock.ticksPerQuarterNote;

		// for each checkpoint in song
		foreach (int checkpoint in this.song.checkpoints) {
			// if past checkpoint, update last checkpoint stored
			if (measure >= checkpoint) this.lastCheckpoint = Mathf.Max(this.lastCheckpoint, checkpoint);
		}
	}

	protected void MissedNote(Song.Note note, Target target) {
		// if last chance
		if (this.lastChance) {
			// compute time in beats of how long it's been since last chance started
			float timeSinceLastChance = (float)(this.clock.time - this.lastChanceStartTime) / (float)this.clock.ticksPerQuarterNote;

			// if less than 1 beat, ignore miss
			if (timeSinceLastChance <= 1) {
				return;
			}
		}

		Debug.Log("miss: " + note.ToString());

		// compute time to reset to based on checkpoint (1 measure before checkpoint)
		int beats = Mathf.Max(this.lastCheckpoint - 1, 0) * 4;
		int ticks = beats * this.clock.ticksPerQuarterNote;
		float time = 60 * (beats / (float)this.clock.tempo);  // seconds

		// disable checkpoints
		// beats = 0;
		// ticks = 0;
		// time = 0;

		// flash target
		target.Flash();

		// if last chance, try again
		if (this.lastChance) {
			// kill recovery sequence
			this.recoveredSequence.Kill();
			this.recoveredSequence = null;

			// kill flash ring sequence
			this.flashRingSequence.Kill();
			this.flashRingSequence = null;

			// animate ring alpha back to default
			DOTween.To(()=> this.player.ringAlpha, x=> this.player.ringAlpha = x, 1f, 0.15f);

			// quickly slow down clock until stopped
			DOTween.To(()=> this.clock.speedScale, x=> this.clock.speedScale = x, 0, 0.15f);

			// simultaneously slow down music until stopped
			DOTween.To(()=> MusicManager.instance.pitch, x=> MusicManager.instance.pitch = x, 0, 0.75f).OnComplete(() => {
				// when stopped...

				// animate backwards to checkpoint (after a delay)
				DOTween.To(()=> this.clock.time, x=> this.clock.time = x, ticks, 1.0f).SetDelay(1f).OnComplete(() => {
					// when reset...

					// reset state
					// disable checkpoints
					// this.ResetToCheckpoint(this.lastCheckpoint);
					this.ResetToCheckpoint(0);

					// disable low pass filter
					MusicManager.instance.lowPass = 0;

					// set music position
					MusicManager.instance.time = time;

					// resume music and clock
					this.clock.speedScale = 1;
					MusicManager.instance.pitch = 1;
				});
			});

			// reset last chance flag
			this.lastChance = false;
		}
		// otherwise, give last chance
		else {
			// animate low pass filter on
			DOTween.To(()=> MusicManager.instance.lowPass, x=> MusicManager.instance.lowPass = x, 1, 0.1f);

			// set last chance flag
			this.lastChance = true;

			// set last chance start time
			this.lastChanceStartTime = this.clock.time;

			// compute flash animation duration (1/2 of a beat) in seconds
			float flashDuration = 0.5f / (float)this.clock.tempo * 60f;

			// flash player ring's alpha
			this.flashRingSequence = DOTween.Sequence();
			this.flashRingSequence.Append(DOTween.To(()=> this.player.ringAlpha, x=> this.player.ringAlpha = x, 0f, flashDuration));
			this.flashRingSequence.Append(DOTween.To(()=> this.player.ringAlpha, x=> this.player.ringAlpha = x, 1f, flashDuration));
			this.flashRingSequence.SetLoops(-1, LoopType.Yoyo);

			// compute interval for recovery (1.25 measures) in seconds
			float interval = 1.25f * 4 * (float)this.clock.tempo / 60;

			// recover after interval
			this.recoveredSequence = DOTween.Sequence();
			this.recoveredSequence.AppendInterval(interval);
			this.recoveredSequence.AppendCallback(()=> {
				// animate low pass filter off
				DOTween.To(()=> MusicManager.instance.lowPass, x=> MusicManager.instance.lowPass = x, 0, 3f);

				// kill flash ring sequence
				this.flashRingSequence.Kill();
				this.flashRingSequence = null;

				// animate ring alpha back to default
				DOTween.To(()=> this.player.ringAlpha, x=> this.player.ringAlpha = x, 1f, 0.15f);

				// reset last chance flag
				this.lastChance = false;
			});
		}
	}

	protected void ResetToCheckpoint(int checkpoint) {
		// compute time to reset to based on checkpoint
		int beats = checkpoint * 4;
		int ticks = beats * this.clock.ticksPerQuarterNote;

		// reset last missed note time
		this.lastMissedNoteTime = 0;

		// get drum track
		List<Song.Note> notes = this.song.tracks[0];

		// for each note
		for (int i = 0; i < notes.Count; i++) {
			// get target
			Target target = this.targets.targets[i];

			// mark target as played if it's before the checkpoint
			target.played = target.playTime < ticks;
		}
	}

	protected UserInput userInput;
	protected Clock clock;
	protected Song song;
	protected Targets targets;
	protected Player player;

	protected long lastMissedNoteTime;
	protected int lastCheckpoint;
	protected bool lastChance;
	protected double lastChanceStartTime;

	protected DG.Tweening.Sequence recoveredSequence;
	protected DG.Tweening.Sequence flashRingSequence;

}
