﻿using UnityEngine;
using System.Collections;
using MidiJack;

public class Stage : Behavior {

	public Transform playerTransform;
	public Canvas ui;

	// player speed (in z-units per minute)
	[System.NonSerialized]
	public float playerSpeed = 1200f;

	public Song song;
	public UserInput userInput;
	public Clock clock;
	public Player player;
	public Targets targets;
	public Evaluation evaluation;

	protected bool initialized = false;
	protected bool started = false;

	public void Play1() {
		this.PlaySong("Sleepless_easy");
	}

	public void Play2() {
		this.PlaySong("Sleepless_medium");
	}

	public void Play3() {
		this.PlaySong("Sleepless_hard");
	}

	public void Play4() {
		this.PlaySong("Ripples");
	}

	public override void Update() {
		// set target framerate
		Application.targetFrameRate = 60;

		// if MusicManager or MidiManager are not ready, do nothing
		if (!MusicManager.instance.ready || !MidiManager.instance.ready) return;

		// if not initialized, initialize
		if (!this.initialized) this.Initialize();

		// if not started music, start
		if (!this.started) {
			StartCoroutine(this.DelayedStart());

			// set started flag
			this.started = true;
		}

#if UNITY_STANDALONE
		// update midi driver
		MidiDriver.Instance.UpdateIfNeeded();
#endif
		// update classes
		this.userInput.Update();
		this.clock.Update();
		this.player.Update();
		this.targets.Update();
		this.evaluation.Update();

		// move camera
		this.MoveCamera();
	}

	protected void Initialize() {
		// initialize classes
		this.song = new Song(MidiManager.instance.midiFile);
		this.userInput = new UserInput(this.song);
		this.clock = new Clock(this.song.tempo, this.song.ticksPerQuarterNote);
		this.player = new Player(this.song, this.playerTransform, this.userInput);
		this.targets = new Targets(this.song, this.clock, this.playerSpeed);
		this.evaluation = new Evaluation(this.userInput, this.clock, this.song, this.targets, this.player);

		// set initialized flag
		this.initialized = true;
	}

	protected void PlaySong(string name) {
		// set song
		MusicManager.instance.songName = name;

		// load midi
		MidiManager.instance.LoadMidi(MidiManager.instance.MidiUrl("drums"));

		// add music stems
		MusicManager.instance.AddStem(MusicManager.instance.StemUrl("master"));

		// hide ui
		this.ui.enabled = false;
	}

	protected IEnumerator DelayedStart() {
		// compute time to next whole number second, rounded up
		float playTime = Mathf.Ceil((float)AudioSettings.dspTime) + 1;

		// play time at computed time
		MusicManager.instance.PlayScheduled(playTime);

		// wait until play time
		while (!MusicManager.instance.playing) {
			yield return null;
		}

		// start clock
		this.clock.running = true;
	}

	protected void MoveCamera() {
		// compute real time from clock time (in minutes)
		double time = (this.clock.time / this.clock.ticksPerQuarterNote) / this.clock.tempo;

		// compute new position based on real time
		Vector3 position = Camera.main.transform.position;
		position.z = (float)time * this.playerSpeed;

		// move camera
		Camera.main.transform.position = position;
	}

}
