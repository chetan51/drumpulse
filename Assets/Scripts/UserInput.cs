﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MidiJack;

public class UserInput {

	// list of indices of instruments played this frame
	public List<int> playedInstruments { get; protected set; }

	public UserInput(Song song) {
		// save parameters
		this.song = song;

		// initialize list of played instruments
		this.playedInstruments = new List<int>();

		// subscribe to midi events
		MidiDriver.Instance.OnKeyDown += this.OnMidiKeyDown;
	}

	public void Update() {
		// clear list of played instruments
		this.playedInstruments.Clear();

		// copy midi played instruments
		this.playedInstruments.AddRange(this.midiPlayedInstruments);

		// clear list of midi played instruments
		this.midiPlayedInstruments.Clear();

		// get instruments map for drum track
		Dictionary<int, int> instrumentsMap = this.song.InstrumentsMap(0);

		// get number of instruments
		int numInstruments = instrumentsMap.Count;

		// for each instrument key
		for (int i = 0; i < numInstruments; i++) {
			// compute instrument key
			int key = i + 1;

			// if instrument key is pressed
			if (Input.GetKeyDown(key.ToString())) {
				// add instrument to list of played instruments
				this.playedInstruments.Add(i);
			}
		}

		// hardcoded keymap
		if (Input.GetKeyDown(KeyCode.LeftCommand)) this.playedInstruments.Add(0);
		if (Input.GetKeyDown(KeyCode.X)) this.playedInstruments.Add(1);
		if (Input.GetKeyDown(KeyCode.Z)) this.playedInstruments.Add(2);
		if (Input.GetKeyDown(KeyCode.LeftAlt)) this.playedInstruments.Add(3);

#if UNITY_STANDALONE
		if (Input.GetMouseButtonDown(0)) {
			// get index of touched instrument
			int idx = this.TouchedInstrumentIdx(Input.mousePosition, numInstruments);

			// add instrument to list of played instruments
			this.playedInstruments.Add(idx);
		}
#else
		foreach (Touch touch in Input.touches) {
			// if touch didn't just start, skip it
			if (touch.phase != TouchPhase.Began) continue;

			// get index of touched instrument
			int idx = this.TouchedInstrumentIdx(touch.position, numInstruments);

			// add instrument to list of played instruments
			this.playedInstruments.Add(idx);
		}
#endif
	}

	// list of indices of instruments played over MIDI before this frame
	protected List<int> midiPlayedInstruments = new List<int>();

	protected int TouchedInstrumentIdx(Vector3 position, int numInstruments) {
		// compute screen center
		Vector3 screenCenter = new Vector3(Screen.width / 2, Screen.height / 2, 0);

		// compute angle between center and position (in radians)
		float angle = Mathf.Atan2(screenCenter.y - position.y, screenCenter.x - position.x);

		// normalize angle to stay between -1 and 1
		float normalized = angle / (2 * Mathf.PI);

		// rotate 90 degrees counter-clockwise
		normalized -= 0.25f;

		// keep angle positive
		if (normalized < 0) normalized += 1f;

		// return instrument index that was touched
		return Mathf.FloorToInt(numInstruments * normalized);
	}

	protected void OnMidiKeyDown(MidiChannel channel, int noteNumber, float velocity) {
		// 38 Snare Drum 1; 40 Snare Drum 2
		if (noteNumber == 38 || noteNumber == 40) {
			this.midiPlayedInstruments.Add(3);
		}
		// 46 Open Hi-hat
		if (noteNumber == 46 || noteNumber == 26) {
			this.midiPlayedInstruments.Add(2);
		}
		// 49 Crash Cymbal 1; 52 Chinese Cymbal; 55 Splash Cymbal; 57 Crash Cymbal 2
		if (noteNumber == 49 || noteNumber == 52 || noteNumber == 55 || noteNumber == 57) {
			this.midiPlayedInstruments.Add(0);
		}
		// 51 Ride Cymbal 1; 53 Ride Bell; 59 Ride Cymbal 2
		if (noteNumber == 51 || noteNumber == 53 || noteNumber == 59) {
			this.midiPlayedInstruments.Add(1);
		}

	    // (noteNumber == 35 || noteNumber == 36) ||  // 35 Bass Drum 2; 36 Bass Drum 1
	    // (noteNumber == 42 || noteNumber == 22) ||  // 42 Closed Hi-hat
	    // (noteNumber == 48 || noteNumber == 50) ||  // 48 High Tom 2; 50 High Tom 1
	    // (noteNumber == 45 || noteNumber == 47) ||  // 45 Mid Tom 2; 47 Mid Tom 1
	    // (noteNumber == 41 || noteNumber == 43) ||  // 41 Low Tom 2; 43 Low Tom 1
	}

	protected Song song;

}
