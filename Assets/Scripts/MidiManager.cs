﻿using UnityEngine;
using System.IO;
using System.Collections;
using NAudio.Midi;

public class MidiManager : Singleton<MidiManager> {

	public bool ready { get; protected set; }
	public MidiFile midiFile { get; protected set; }

	public string MidiUrl(string name) {
		return System.IO.Path.Combine(MusicManager.instance.songPath, name + ".mid");
	}

	public void LoadMidi(string url) {
#if UNITY_ANDROID
		StartCoroutine(this.LoadFile(url));
#else
		// load midi
		this.midiFile = new MidiFile(url, true);

		// mark as ready
		this.ready = true;
#endif
	}

	// bullshit for android
	protected IEnumerator LoadFile(string url) {
		var www = new WWW(url);
		yield return www;

		if (!string.IsNullOrEmpty(www.error)) {
			Debug.LogError("Can't load midi file.");
			yield break;
		}

		string path = Application.persistentDataPath + "/drums.mid";
		Debug.Log(path);

		FileStream stream = new FileStream(path, FileMode.Create);
	    BinaryWriter writer = new BinaryWriter(stream);

	    writer.Write(www.bytes);

	    writer.Close();
	    stream.Close();

		// load midi
		this.midiFile = new MidiFile(path, true);

		// mark as ready
		this.ready = true;
	}

}
