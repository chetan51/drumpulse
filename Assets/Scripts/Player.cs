﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;
using DG.Tweening;

public class Player {

	// segments per instrument
	public int segmentsPerInstrument = 25;

	// default alpha of pad
	public float padDefaultAlpha = 0.25f;

	// alpha of ring
	public float ringAlpha {
		get {
			// return alpha of first ring
			return this.ring[0].color.a;
		}
		set {
			// for each ring
			foreach (VectorLine line in this.ring) {
				// set alpha of line
				Color color = line.color;
				color.a = value;
				line.color = color;
			}
		}
	}

	public Player(Song song, Transform transform, UserInput userInput) {
		// save parameters
		this.song = song;
		this.transform = transform;
		this.userInput = userInput;

		// line radii
		float padRadius = 8f;
		float ringRadius = 3f;

		// ring width
		float ringWidth = 3f;

		// compute pad width from radii
		Vector3 padTop = Camera.main.WorldToScreenPoint(new Vector3(0, padRadius, 0));
		Vector3 ringTop = Camera.main.WorldToScreenPoint(new Vector3(0, ringRadius, 0));
		float padWidth = (padTop.y - (ringTop.y + ringWidth)) * 2;

		// create pads
		this.pads = this.CreateLines("Player-Pads", padWidth, padRadius, this.padDefaultAlpha, Resources.Load("Materials/Player_Pads") as Material);

		// create ring
		this.ring = this.CreateLines("Player-Ring", ringWidth, ringRadius, 1f, Resources.Load("Materials/Player_Ring") as Material);
	}

	public void Update() {
		// process user input
		this.ProcessUserInput();

		// draw pads
		foreach (VectorLine line in this.pads) {
			line.Draw();
		}

		// draw ring
		foreach (VectorLine line in this.ring) {
			line.Draw();
		}
	}

	protected List<VectorLine> CreateLines(string name, float width, float radius, float alpha, Material material) {
		// initialize vector line list
		List<VectorLine> lines = new List<VectorLine>();

		// get number of instruments
		int numInstruments = this.song.InstrumentsMap(0).Count;

		// get instrument colors
		List<Color> instrumentColors = this.song.instrumentColors;

		// for each instrument
		for (int i = 0; i < numInstruments; i++) {
			// initialize new vector line
			VectorLine line = new VectorLine(name, new List<Vector3>(), width, LineType.Discrete, Joins.Weld);

			// add line to list
			lines.Add(line);

			// compute num points
			int numPoints = this.segmentsPerInstrument * 2;

			// resize line
			line.Resize(numPoints);

			// compute angle span of note
			float angleSpan = 360f / numInstruments;

			// compute start and end angle for instrument
			float startAngle = angleSpan * i;
			float endAngle = startAngle + angleSpan;

			// make an arc
			line.MakeArc(Vector3.zero, radius, radius, startAngle, endAngle);

			// get color of instrument
			Color color = instrumentColors[i];

			// set alpha
			color.a = alpha;

			// set line color
			line.color = color;

			// set line transform
			line.drawTransform = this.transform;

			// set line material
			line.material = material;
		}

		return lines;
	}

	protected void ProcessUserInput() {
		// get instrument colors
		List<Color> instrumentColors = this.song.instrumentColors;

		// for each played instrument
		foreach (int i in this.userInput.playedInstruments) {
			// get pad for instrument
			VectorLine pad = this.pads[i];

			// get color for instrument
			Color color = instrumentColors[i];

			// save original color
			Color originalColor = color;
			originalColor.a = this.padDefaultAlpha;

			// flash pad to instrument color and back
			DG.Tweening.Sequence sequence = DOTween.Sequence();
			sequence.Append(DOTween.To(()=> pad.color, x=> pad.color = x, color, 0.1f));
			sequence.Append(DOTween.To(()=> pad.color, x=> pad.color = x, originalColor, 0.15f));
		}
	}

	protected Song song;
	protected Transform transform;
	protected UserInput userInput;

	protected List<VectorLine> ring;
	protected List<VectorLine> pads;

}
