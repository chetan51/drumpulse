﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;
using DG.Tweening;

public class Target {

	public float passedAlpha = 0.25f;
	public bool fadeAfterPassed = false;

	public long playTime { get; protected set; }

	public Target(long playTime, Clock clock, string name, Material material, float depth, float startAngle, float endAngle, Color color, float width, int numSegments) {
		// save parameters
		this.playTime = playTime;
		this.clock = clock;
		this.name = name;
		this.material = material;
		this.depth = depth;
		this.startAngle = startAngle;
		this.endAngle = endAngle;
		this.color = color;
		this.width = width;
		this.numSegments = numSegments;

		// create line
		this.CreateLine();
	}

	public void Update() {
		// set alpha based on distance to camera
		float distanceScale = Mathf.Abs(Camera.main.transform.position.z - this.depth) / 150f;
		float alpha = Mathf.Lerp(1f, 0f, distanceScale);

		// if played, dim color after note has passed
		Color color = this.color;
		// color.a = (this.played && this.clock.time > this.playTime) ? this.passedAlpha : alpha;
		color.a = (this.played) ? this.passedAlpha : alpha;

		// if fadeAfterPassed is true, implement it
		color.a = (this.fadeAfterPassed && this.clock.time > this.playTime) ? this.passedAlpha : color.a;

		// set line color
		this.line.color = color;

		// update width based on distance to camera
		this.line.lineWidth = Mathf.Lerp(this.width, this.width / 3, distanceScale);

		// draw line
		this.line.Draw();
	}

	public bool visible {
		get {
			return this.line.active;
		}
		set {
			this.line.active = value;
		}
	}

	public bool played;

	public void Flash() {
		Color originalColor = this.color;

		// flash from white to original color 5 times
		DG.Tweening.Sequence sequence = DOTween.Sequence();
		sequence.Append(DOTween.To(()=> this.color, x=> this.color = x, Color.white, 0.2f));
		sequence.Append(DOTween.To(()=> this.color, x=> this.color = x, originalColor, 0.2f));
		sequence.SetLoops(5, LoopType.Yoyo);
	}

	protected void CreateLine() {
		// initialize vector line
		this.line = new VectorLine("Target-" + this.name, new List<Vector3>(), this.width, LineType.Discrete);

		// set line material
		this.line.material = this.material;
		// this.line.material = Resources.Load("Materials/Target") as Material;

		// resize line
		this.line.Resize(this.numSegments);

		// compute position
		Vector3 position = new Vector3(0, 0, this.depth);

		// TODO: refactor this
		// move position by player amount
		position.z += 10;

		// make an arc
		this.line.MakeArc(position, 3f, 3f, this.startAngle, this.endAngle);

		// // automatically redraw every frame
		// this.line.Draw3DAuto();
	}

	protected Clock clock;
	protected string name;
	protected Material material;
	protected float depth;
	protected float startAngle;
	protected float endAngle;
	protected Color color;
	protected float width;
	protected int numSegments;

	protected VectorLine line;
	protected List<Color32> colors = new List<Color32>();

	protected bool _played;

}
