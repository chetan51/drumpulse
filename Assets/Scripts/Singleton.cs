﻿using UnityEngine;
using System.Collections;

public class Singleton<TBehavior> : Behavior where TBehavior : Behavior {

	public static TBehavior instance {
		get {
			// if instance hasn't been set, find the object in the scene and set it
			if (_instance == null) {
				_instance = GameObject.FindObjectOfType<TBehavior>();
				Singleton<TBehavior> instanceSingleton = _instance as Singleton<TBehavior>;
				instanceSingleton.AwakeSingleton();
			}

			// if the object in the scene couldn't be found, log an error and return null
			if (_instance == null) {
				Debug.LogError(string.Format("An instance of type {0} is needed in the scene, but none was found.", typeof(TBehavior)));
				return null;
			}

			return _instance;
		}
	}

	public virtual void AwakeSingleton() {}

	protected static TBehavior _instance;

}
