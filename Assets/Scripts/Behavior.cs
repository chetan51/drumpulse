﻿using UnityEngine;
using System.Collections;

public class Behavior : MonoBehaviour {

	[System.NonSerialized]
	public bool dirty = true;

	public void Ready() {
		// if dirty (either first time active, or respawned by Pool)
		if (this.dirty) {
			// trigger event
			this.OnActivate();

			// clear dirty flag
			this.dirty = false;
		}
	}

	public virtual void Awake() {}

	public virtual void OnEnable() {
		this.Ready();
	}

	public virtual void Start() {}

	public virtual void Update() {}

	public virtual void LateUpdate() {}

	public virtual void OnDisable() {}

	public virtual void OnDestroy() {}

	public virtual void OnActivate() {}

	public virtual void OnReclaim() {}

}
