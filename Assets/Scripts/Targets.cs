﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Targets {

	// cull targets that are more than some time away
	public float cullTime = 0.15f;  // minutes

	public List<Target> targets { get; protected set; }

	public Targets(Song song, Clock clock, float playerSpeed) {
		// save parameters
		this.song = song;
		this.clock = clock;
		this.playerSpeed = playerSpeed;

		// initialize targets list
		this.targets = new List<Target>();

		// load material
		this.material = Resources.Load("Materials/Target") as Material;

		// create grid
		this.CreateGrid();

		// create targets from song
		this.CreateTargets();
	}

	public void Update() {
		// get drum track
		List<Song.Note> notes = this.song.tracks[0];

		// for each grid line
		foreach (Target gridLine in this.grid) {
			// compute time difference from current time to note start time
			float timeDiff = Mathf.Abs((float)(this.clock.time - gridLine.playTime));

			// convert to real time
			float realtimeDiff = (float)((timeDiff / this.clock.ticksPerQuarterNote) / this.clock.tempo);

			// if more than cullTime away, hide target, otherwise show
			gridLine.visible = realtimeDiff <= this.cullTime;

			// mark grid as fadeAfterPassed so it fades after passing current time
			gridLine.fadeAfterPassed = true;

			// update grid line
			gridLine.Update();
		}

		// for each note
		for (int i = 0; i < notes.Count; i++) {
			// get note
			Song.Note note = notes[i];

			// get target
			Target target = this.targets[i];

			// compute time difference from current time to note start time
			float timeDiff = Mathf.Abs((float)(this.clock.time - note.startTime));

			// convert to real time
			float realtimeDiff = (float)((timeDiff / this.clock.ticksPerQuarterNote) / this.clock.tempo);

			// if more than cullTime away, hide target, otherwise show
			target.visible = realtimeDiff <= this.cullTime;

			// update target
			target.Update();
		}
	}

	protected void CreateTargets() {
		// get instruments map for drum track
		Dictionary<int, int> instrumentsMap = this.song.InstrumentsMap(0);

		// get number of instruments
		int numInstruments = instrumentsMap.Count;

		// get drum track
		List<Song.Note> notes = this.song.tracks[0];

		// get instrument colors
		List<Color> instrumentColors = this.song.instrumentColors;

		// for each note
		for (int i = 0; i < notes.Count; i++) {
			// get note
			Song.Note note = notes[i];

			// compute real time from note time (in minutes)
			float time = ((float)note.startTime / this.song.ticksPerQuarterNote) / (float)this.song.tempo;

			// compute depth for target based on real time
			float depth = time * this.playerSpeed;

			// compute angle span of note
			float angleSpan = 360f / numInstruments;

			// compute start and end angle for note
			int instrumentIdx = instrumentsMap[note.noteNumber];
			float startAngle = angleSpan * instrumentIdx;
			float endAngle = startAngle + angleSpan;

			// get color
			Color color = instrumentColors[instrumentIdx];

			// create target for note
			Target target = new Target(note.startTime, this.clock, i.ToString(), this.material, depth, startAngle, endAngle, color, 10f, 50);

			// add it to the list of targets
			this.targets.Add(target);

			// // color of grid line
			// Color gridColor = new Color(0.2f, 0.2f, 0.2f);

			// // create grid line for note
			// Target gridLine = new Target(note.startTime, this.clock, "Grid-" + i.ToString(), this.material, depth, 0, 360f, gridColor, 3f, 100);

			// // set passed alpha parameter
			// gridLine.passedAlpha = 0;

			// // add it to the grid
			// this.grid.Add(gridLine);
		}
	}

	protected void CreateGrid() {
		// get duration of drum track
		long duration = this.song.Duration(0);

		// number of grid lines per beat
		int numPerBeat = 1;

		// compute number of lines
		long numLines = duration / this.song.ticksPerQuarterNote * numPerBeat;

		// for each line
		for (int i = 0; i < numLines; i++) {
			// compute play time of grid line
			long playTime = i * this.song.ticksPerQuarterNote / numPerBeat;

			// compute real time from play time (in minutes)
			float time = ((float)playTime / this.song.ticksPerQuarterNote) / (float)this.song.tempo;

			// compute depth for grid line based on real time
			float depth = time * this.playerSpeed;

			// color of grid line
			Color color = new Color(0.2f, 0.2f, 0.2f);

			// create grid line for note
			Target gridLine = new Target(playTime, this.clock, "Grid-" + i.ToString(), this.material, depth, 0, 360f, color, 3f, 100);

			// set passed alpha parameter
			gridLine.passedAlpha = 0;

			// add it to the grid
			this.grid.Add(gridLine);
		}
	}

	protected Song song;
	protected Clock clock;
	protected float playerSpeed;

	protected Material material;

	protected List<Target> grid = new List<Target>();

}
