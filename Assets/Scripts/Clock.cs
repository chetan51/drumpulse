﻿using UnityEngine;
using System.Collections;

public class Clock {

	// offset time to account for audio latency
#if UNITY_STANDALONE
	public float timeOffset = 0;  // no offset
#elif UNITY_ANDROID
	public float timeOffset = .075f / 60f;  // .075 seconds
#else
	public float timeOffset = .26f / 60f;  // .26 seconds
#endif

	public bool running = false;
	public float speedScale = 1;  // scales rate of time

	public double tempo;  // beats per minute
	public int ticksPerQuarterNote;

	// time in ticks (after offset)
	public double time {
		get {
			// return time offset'd by timeOffset (in ticks)
			return this._time - (this.timeOffset * this.tempo * this.ticksPerQuarterNote);
		}
		set {
			this._time = value;
		}
	}

	public Clock(double tempo, int ticksPerQuarterNote) {
		// save parameters
		this.tempo = tempo;
		this.ticksPerQuarterNote = ticksPerQuarterNote;

		// set last DSP time
		this.lastDspTime = AudioSettings.dspTime;
	}

	public void Update() {
		// update time
		this.UpdateTime();

		// update last DSP time
		this.lastDspTime = AudioSettings.dspTime;
	}

	protected double _time;
	protected double lastDspTime;

	protected void UpdateTime() {
		// if not running, do nothing
		if (!running) return;

		// compute time difference
		double deltaDspTime = AudioSettings.dspTime - this.lastDspTime;

		// compute number of ticks elapsed
		double beatsPerSecond = this.tempo / 60f;
		double deltaQuarterNotes = deltaDspTime * beatsPerSecond;
		double deltaTicks = deltaQuarterNotes * this.ticksPerQuarterNote;

		// update internal time
		this._time += deltaTicks * this.speedScale;
	}

}
