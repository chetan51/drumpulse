﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NAudio.Midi;

public class Song {

	public struct Note {
		public int channel;
		public int noteNumber;
		public int velocity;
		public long startTime;
		public long endTime;

        public Note(int channel, int noteNumber, int velocity) {
        	// save parameters
        	this.channel = channel;
        	this.noteNumber = noteNumber;
        	this.velocity = velocity;

        	// set empty values
        	this.startTime = 0;
        	this.endTime = 0;
        }

        public override string ToString() {
        	return string.Format("[Note] channel: {0} / note number: {1} / velocity: {2} / start time: {3} / end time: {4}",
        	                     this.channel, this.noteNumber, this.velocity, this.startTime, this.endTime);
        }
	}

	public List<List<Note>> tracks = new List<List<Note>>();

	public MidiFile midiFile;

	public Song(MidiFile midiFile) {
		// save parameters
		this.midiFile = midiFile;

		// extract notes
		this.ExtractNotes();
	}

	public double tempo {
		get {
			// TODO: Hardcoded for song, get this from midi file
			if (MusicManager.instance.songName == "Sleepless_easy" ||
			    MusicManager.instance.songName == "Sleepless_medium" ||
				MusicManager.instance.songName == "Sleepless_hard") {
				return 87.5f;
			}
			if (MusicManager.instance.songName == "Ripples") {
				return 160f;
			}

			// get tempo event from midi file
			TempoEvent tempoEvent = this.midiFile.Events[0][0] as TempoEvent;

			// return tempo from event
			return tempoEvent.Tempo;
		}
	}

	public int ticksPerQuarterNote {
		get {
			// get it from midi file
			return this.midiFile.DeltaTicksPerQuarterNote;
		}
	}

	public List<int> checkpoints {  // in measures
		get {
			// TODO: Temporary hardcode specific to song
			if (MusicManager.instance.songName == "Sleepless_easy" ||
			    MusicManager.instance.songName == "Sleepless_medium" ||
				MusicManager.instance.songName == "Sleepless_hard") {
				return new List<int> {2, 4, 12, 20, 28, 36, 44, 60};
			}
			if (MusicManager.instance.songName == "Ripples") {
				return new List<int> {4, 8, 16, 24, 32, 40, 48, 56, 65};
			}

			return new List<int> {};
		}
	}

	public List<Color> instrumentColors {
		get {
			return new List<Color> {
				new Color32(22,147,165,255),
				new Color32(251,184,41,255),
				new Color32(209,56,29,255),
				new Color32(228,95,142,255),
				new Color32(18,207,155,255),
				new Color32(11,160,244,255),
				new Color32(143,227,126,255),
				new Color32(6,96,135,255)
			};
		}
	}

	public Dictionary<int, int> InstrumentsMap(int trackIdx) {
		// get track
		List<Song.Note> notes = this.tracks[trackIdx];

		// initialize mapping between note number and instrument index
		Dictionary<int, int> instrumentsMap = new Dictionary<int, int>();

		// initialize counter of instruments
		int numInstruments = 0;

		// for each note
		foreach (Song.Note note in notes) {
			// if note has been mapped, skip it
			if (instrumentsMap.ContainsKey(note.noteNumber)) continue;

			// add instrument mapping
			instrumentsMap[note.noteNumber] = numInstruments;

			// increment num instruments
			numInstruments++;
		}

		// TODO: Temporary hardcode specific to song
		if (MusicManager.instance.songName == "Sleepless_easy" ||
		    MusicManager.instance.songName == "Sleepless_medium" ||
			MusicManager.instance.songName == "Sleepless_hard") {
			instrumentsMap.Clear();
			instrumentsMap[36] = 0;
			instrumentsMap[38] = 1;
			instrumentsMap[42] = 3;
			instrumentsMap[46] = 2;
		}
		if (MusicManager.instance.songName == "Ripples") {
			instrumentsMap.Clear();
			instrumentsMap[36] = 2;
			instrumentsMap[38] = 1;
			instrumentsMap[39] = 3;
			instrumentsMap[42] = 0;
		}

		// return mapping between note number and instrument index
		return instrumentsMap;
	}

	public long Duration(int trackIdx) {
		// get track
		List<Song.Note> notes = this.tracks[trackIdx];

		// get last note
		Song.Note lastNote = notes[notes.Count - 1];

		// return start time of last note
		return lastNote.startTime;
	}

	protected void ExtractNotes() {
		// convenience variables
		MidiEventCollection eventCollection = this.midiFile.Events;

		// for each track
		for (int i = 0; i < eventCollection.Tracks; i++) {
			// initialize list of notes
			List<Note> notes = new List<Note>();

			// initialize list of outstanding On notes
			List<Note> onNotes = new List<Note>();

			// get events
			IList<MidiEvent> events = eventCollection[i];

			// for each event
			foreach (MidiEvent midiEvent in events) {
				// if event is note on or off
				if (MidiEvent.IsNoteOn(midiEvent) || MidiEvent.IsNoteOff(midiEvent)) {
					// cast to note event
					NoteEvent noteEvent = midiEvent as NoteEvent;

					// if event is note on
					if (MidiEvent.IsNoteOn(midiEvent)) {
						// create new note
						Note note = new Note(noteEvent.Channel, noteEvent.NoteNumber, noteEvent.Velocity);

						// set note start time
						note.startTime = noteEvent.AbsoluteTime;

						// add to list of outstanding On notes
						onNotes.Add(note);
					}

					// if event is note off
					if (MidiEvent.IsNoteOff(midiEvent)) {
						// find index of corresponding outstanding On note
						int idx = this.IndexOfNote(onNotes, noteEvent);

						// if note wasn't found, log error and skip it
						if (idx == -1) {
							Debug.LogError("Extract notes from MIDI file: Corresponding On note not found for Off note.");
						}

						// get note
						Note note = onNotes[idx];

						// remove note from list of outstanding On notes
						onNotes.Remove(note);

						// set note end time
						note.endTime = noteEvent.AbsoluteTime;

						// add note to notes list
						notes.Add(note);
					}
				}
			}

			// if list of outstanding On notes is not empty by now, report error
			if (onNotes.Count > 0) Debug.LogError(string.Format("Extract notes from MIDI file: {0} On notes didn't have a corresponding Off note.", onNotes.Count));

			// add notes to track
			this.tracks.Add(notes);
		}
	}

	protected int IndexOfNote(List<Note> notes, NoteEvent noteEvent) {
		// for each note
		for (int i = 0; i < notes.Count; i++) {
			// get note
			Note note = notes[i];

			// if note is same channel and note number, return index
			if (note.channel == noteEvent.Channel && note.noteNumber == noteEvent.NoteNumber) return i;
		}

		// note wasn't found, return -1
		return -1;
	}

}
