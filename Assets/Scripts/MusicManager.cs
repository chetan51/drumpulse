﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicManager : Singleton<MusicManager> {

	public string songName;

	public string songPath {
		get {
			return System.IO.Path.Combine(Application.streamingAssetsPath, this.songName);
		}
	}

	public bool ready {
		get {
			// initialize ready flag
			bool ready = true;

			// for each stem
			foreach (Stem stem in this.stems) {
				// if stem isn't ready, update flag
				if (!stem.ready) ready = false;
			}

			// return ready flag
			return ready;
		}
	}

	public bool playing {
		get {
			return this.audioSources[0].time > 0;
		}
	}

	public float time {
		get {
			return this.audioSources[0].time;
		}
		set {
			// set time of all audio sources to value
			foreach (AudioSource audioSource in this.audioSources) {
				audioSource.time = value;
			}
		}
	}

	public float pitch {
		get {
			// return pitch of first audio source
			return this.audioSources[0].pitch;
		}
		set {
			// set time of all audio sources to value
			foreach (AudioSource audioSource in this.audioSources) {
				audioSource.pitch = value;
			}
		}
	}

	public float duration {
		get {
			// return length of first clip
			return this.audioSources[0].clip.length;
		}
	}

	public float lowPass {
		get {
			return this._lowPass;
		}
		set {
			this._lowPass = value;

			// for each low pass filter
			foreach (AudioLowPassFilter filter in this.filters) {
				// set cutoff frequency based on value
				filter.cutoffFrequency = Mathf.Lerp(22000f, 2300f, value);
			}
		}
	}

	public string StemUrl(string name) {
#if UNITY_STANDALONE
		return "file://" + System.IO.Path.Combine(this.songPath, name + ".ogg");
#elif UNITY_ANDROID
		return System.IO.Path.Combine(this.songPath, name + ".mp3");
#else
		return "file://" + System.IO.Path.Combine(this.songPath, name + ".mp3");
#endif
	}

	public void AddStem(string url) {
		// create new stem
		Stem stem = new Stem(url);

		// add to list of stems
		this.stems.Add(stem);

		// start downloading stem
		StartCoroutine(this.StartDownload(url, this.stems.Count - 1));
	}

	public void PlayScheduled(double dspTime) {
		// for each stem
		foreach (Stem stem in this.stems) {
		    // create audio source object
		    GameObject obj = Instantiate(Resources.Load("Prefabs/StemAudioSource")) as GameObject;

		    // name it
		    obj.name = "StemAudioSource: " + stem.url;

		    // parent it
		    obj.transform.SetParent(this.transform, false);

		    // get audio source
		    AudioSource audioSource = obj.GetComponent<AudioSource>();

		    // set its clip
		    audioSource.clip = stem.clip;

		    // add it to list
		    this.audioSources.Add(audioSource);

		    // get low pass filter
			AudioLowPassFilter filter = audioSource.GetComponent<AudioLowPassFilter>();

		    // add it to list
		    this.filters.Add(filter);
		}

		// for each audio source
		foreach (AudioSource audioSource in this.audioSources) {
		    Debug.Log(Time.realtimeSinceStartup + " / " + "playing stem");

		    // play it
		    audioSource.PlayScheduled(dspTime);
		}
	}

	public void Play() {
		// play now
		this.PlayScheduled(AudioSettings.dspTime);
	}

	protected struct Stem {
		public string url;
		public bool ready;
		public AudioClip clip;

		public Stem(string url) {
			this.url = url;
			this.ready = false;
			this.clip = null;
		}
	}

	protected List<Stem> stems = new List<Stem>();
	protected List<AudioSource> audioSources = new List<AudioSource>();
	protected List<AudioLowPassFilter> filters = new List<AudioLowPassFilter>();

	protected float _lowPass;

	protected IEnumerator StartDownload(string url, int stemIdx) {
		// download data
	    WWW www = new WWW(url);
	    yield return www;

	    // get stem
	    Stem stem = this.stems[stemIdx];

		// save downloaded data to stem
		AudioClip clip = www.GetAudioClip(false, true);
	    stem.clip = clip;

	    // if clip isn't ready to play, wait until it is
	    if (!clip.isReadyToPlay) yield return null;

	    // mark stem as ready
	    stem.ready = true;

	    // update stem list
	    this.stems[stemIdx] = stem;

	    Debug.Log(Time.realtimeSinceStartup + " / " + "ready: " + url);
		// Resources.UnloadUnusedAssets();
	}

}
